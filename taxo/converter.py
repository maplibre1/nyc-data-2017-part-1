import pandas as pd

months = ['01','02','03','04','05','06','07','08','09','10','11','12']

for month in months:
  df = pd.read_parquet('yellow_tripdata_2017-'+month+'.parquet')
  df.to_csv('yellow_tripdata_2017-'+month+'.csv',index=False)